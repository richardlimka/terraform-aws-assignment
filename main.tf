resource "aws_iam_user" "readonly_user" {
  name = "readonly_user"
  tags = {
      Description = "User with read only permission to all AWS resources"
  }
}